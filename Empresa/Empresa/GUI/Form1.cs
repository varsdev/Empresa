﻿using Empresa.Entities;
using Empresa.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form frm = new FrmColaborador(false);
            frm.Show();
        }

        private void FrmMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form frm = new FrmColaborador(true);
            frm.Show();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form frm = new FrmReportes();
            frm.Show();
        }
    }
}
