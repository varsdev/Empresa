﻿using Empresa.BO;
using Empresa.DAO;
using Empresa.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa.GUI
{
    public partial class FrmColaborador : Form
    {
        bool colab;

        public FrmColaborador(bool col)
        {
            InitializeComponent();
            CenterToScreen();
            colab = col;
            cargarDatos();
            if (col)
            {
                lblTitulo.Text = "Editor de Colaboradores";
                btnAgregar.Text = "Editar";
                btnBuscar.Visible = true;
                lblCedula.Text = "Digite la cedula";
                btnAgregar.Enabled = false;
            }
        }

        /// <summary>
        /// Carga los datos en los combobox
        /// </summary>
        public void cargarDatos()
        {
            PuestoDAO pdao = new PuestoDAO();
            foreach (Puesto item in pdao.ConsultarPuestos())
            {
                cbxPuesto.Items.Add(item);
            }

            DepartamentoDAO ddao = new DepartamentoDAO();
            foreach (Departamento item in ddao.ConsultarDepartamentos())
            {
                cbxDepartamento.Items.Add(item);
            }

        }

        private void FrmColaborador_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form frm = new FrmMenu();
            frm.Show();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!colab)
                {
                    Colaborador c = new Colaborador();
                    c.sgCedula = txtCedula.Text.Trim();
                    c.sgNombre = txtNombre.Text.Trim();
                    if (rdbMasculino.Checked)
                    {
                        c.sgGenero = 'M';
                    }
                    else if (rdbFemenino.Checked)
                    {
                        c.sgGenero = 'F';
                    }
                    c.sgFechaNacimiento = dtpNacimiento.Value.Date;
                    ColaboradorBO cbo = new ColaboradorBO();
                    c.sgEdad = cbo.calcEdad(c.sgFechaNacimiento);
                    c.sgFechaIngreso = dtpIngreso.Value.Date;
                    c.sgNivelIngles = cbxIngles.Text;
                    Departamento dep = (Departamento)cbxDepartamento.SelectedItem;
                    c.sgDepartamento = dep;
                    Puesto pue = (Puesto)cbxPuesto.SelectedItem; 
                    c.sgPuesto = pue;
                    ColaboradorDAO cdao = new ColaboradorDAO();
                    cdao.InsertarDatos(c);
                    MessageBox.Show("Colaborador registrado");
                }
                else
                {
                    ColaboradorDAO cdao = new ColaboradorDAO();
                    Colaborador c = new Colaborador();
                    c = cdao.ConsultarDatos(txtCedula.Text.Trim());
                    c.sgNombre = txtNombre.Text.Trim();
                    c.sgFechaIngreso = dtpIngreso.Value.Date;
                    c.sgNivelIngles = cbxIngles.Text;
                    Departamento dep = (Departamento)cbxDepartamento.SelectedItem;
                    c.sgDepartamento = dep;
                    Puesto pue = (Puesto)cbxPuesto.SelectedItem;
                    c.sgPuesto = pue;
                    cdao.ModificarDatos(c);
                    MessageBox.Show("Colaborador editado");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Intente nuevamente");
            }      
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ColaboradorDAO cdao = new ColaboradorDAO();
                Colaborador c = new Colaborador();
                c = cdao.ConsultarDatos(txtCedula.Text.Trim());
                if (c != null)
                {
                    txtNombre.Text = c.sgNombre;
                    txtEdad.Text = c.sgEdad.ToString();
                    if (c.gGenero == "Masculino")
                    {
                        rdbMasculino.Checked = true;
                    }
                    else if (c.gGenero == "Femenino")
                    {
                        rdbFemenino.Checked = true;
                    }
                    dtpNacimiento.Enabled = false;
                    rdbFemenino.Enabled = false;
                    rdbMasculino.Enabled = false;
                    dtpNacimiento.Value = c.sgFechaNacimiento;
                    dtpIngreso.Value = c.sgFechaIngreso;
                    cbxDepartamento.SelectedText = c.sgDepartamento.ToString();
                    cbxPuesto.SelectedText = c.sgPuesto.ToString();
                    cbxIngles.SelectedText = c.sgNivelIngles.ToString();

                    txtCedula.Enabled = false;
                    btnBuscar.Enabled = false;
                    btnAgregar.Enabled = true;
                    MessageBox.Show("Colaborador encontrado");
                    Console.WriteLine(c.ToString());
                }
                else
                {
                    MessageBox.Show("Colaborador no encontrado");
                    txtCedula.ResetText();
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Intente nuevamente");

            }
        }

        private void dtpNacimiento_Leave(object sender, EventArgs e)
        {
            ColaboradorBO cbo = new ColaboradorBO();
            txtEdad.Text = (cbo.calcEdad(dtpNacimiento.Value.Date)).ToString();
        }
    }
}
