﻿using Empresa.DAO;
using Empresa.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa.GUI
{
    public partial class FrmReportes : Form
    {
        public FrmReportes()
        {
            InitializeComponent();
            CenterToScreen();
            cargarDatos();          
        }

        /// <summary>
        /// Carga los datos en combobox
        /// </summary>
        public void cargarDatos()
        {           
            DepartamentoDAO ddao = new DepartamentoDAO();
            foreach (Departamento item in ddao.ConsultarDepartamentos())
            {
                cbxDep.Items.Add(item);
            }
            foreach (Departamento item in ddao.ConsultarDepartamentos())
            {
                cbxDepEdad.Items.Add(item);
            }
        }

        private void FrmReportes_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form frm = new FrmMenu();
            frm.Show();
        }

        private void btnBuscarDep_Click(object sender, EventArgs e)
        {
            try
            {
                listDep.HorizontalScrollbar = true;
                DepartamentoDAO ddao = new DepartamentoDAO();
                Departamento d = (Departamento)cbxDep.SelectedItem;

                listDep.Items.Clear();
                foreach (var item in ddao.ConsultaDepList(d.sgId))
                {
                    listDep.Items.Add(item);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Intente nuevamente");
            }
        }

        private void btnBuscarIng_Click(object sender, EventArgs e)
        {
            try
            {
                listMujIng.HorizontalScrollbar = true;
                ColaboradorDAO cdao = new ColaboradorDAO();
                listMujIng.Items.Clear();

                foreach (var item in cdao.ConsultaNivIngFList(cbxIngles.Text))
                {
                    listMujIng.Items.Add(item);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Intente nuevamente");
            }
        }

        private void btnFecha_Click(object sender, EventArgs e)
        {
            try
            {
                listFecha.HorizontalScrollbar = true;
                ColaboradorDAO cdao = new ColaboradorDAO();
                DateTime min = dtpFeMen.Value.Date;
                DateTime may = dtpFeMay.Value.Date;
                listFecha.Items.Clear();

                foreach (var item in cdao.ConsultaFechas(min, may))
                {
                    listFecha.Items.Add(item);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Intente nuevamente");
            }
        }

        private void btnBuscarEdad_Click(object sender, EventArgs e)
        {
            try
            {
                listEdad.HorizontalScrollbar = true;
                ColaboradorDAO cdao = new ColaboradorDAO();
                int min = int.Parse(txtEdadMin.Text.Trim());
                int max = int.Parse(txtEdadMax.Text.Trim());
                Departamento d = (Departamento)cbxDepEdad.SelectedItem;
                listEdad.Items.Clear();

                foreach (var item in cdao.ConsultaGeEdDep(min, max, d.sgId))
                {
                    listEdad.Items.Add(item);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Intente nuevamente");
            }          
        }

        private void tbpDepEsp_Leave(object sender, EventArgs e)
        {
            cbxDep.ResetText();
            listDep.Items.Clear();
        }

        private void tbpMujIng_Leave(object sender, EventArgs e)
        {
            cbxIngles.ResetText();
            listMujIng.Items.Clear();
        }

        private void tbpRanFecIng_Leave(object sender, EventArgs e)
        {
            dtpFeMay.ResetText();
            dtpFeMen.ResetText();
            listFecha.Items.Clear();
        }

        private void tbpHMEda_Leave(object sender, EventArgs e)
        {
            txtEdadMax.ResetText();
            txtEdadMin.ResetText();
            cbxDepEdad.ResetText();
            listEdad.Items.Clear();
        }
    }
}
