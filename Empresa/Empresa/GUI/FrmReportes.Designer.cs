﻿namespace Empresa.GUI
{
    partial class FrmReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbpDepEsp = new System.Windows.Forms.TabPage();
            this.cbxDep = new System.Windows.Forms.ComboBox();
            this.btnBuscarDep = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbpMujIng = new System.Windows.Forms.TabPage();
            this.tbpRanFecIng = new System.Windows.Forms.TabPage();
            this.tbpHMEda = new System.Windows.Forms.TabPage();
            this.listDep = new System.Windows.Forms.ListBox();
            this.listMujIng = new System.Windows.Forms.ListBox();
            this.cbxIngles = new System.Windows.Forms.ComboBox();
            this.btnBuscarIng = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.listFecha = new System.Windows.Forms.ListBox();
            this.btnFecha = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFeMen = new System.Windows.Forms.DateTimePicker();
            this.dtpFeMay = new System.Windows.Forms.DateTimePicker();
            this.listEdad = new System.Windows.Forms.ListBox();
            this.cbxDepEdad = new System.Windows.Forms.ComboBox();
            this.btnBuscarEdad = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEdadMin = new System.Windows.Forms.TextBox();
            this.txtEdadMax = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tbpDepEsp.SuspendLayout();
            this.tbpMujIng.SuspendLayout();
            this.tbpRanFecIng.SuspendLayout();
            this.tbpHMEda.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbpDepEsp);
            this.tabControl1.Controls.Add(this.tbpMujIng);
            this.tabControl1.Controls.Add(this.tbpRanFecIng);
            this.tabControl1.Controls.Add(this.tbpHMEda);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(-1, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(803, 449);
            this.tabControl1.TabIndex = 0;
            // 
            // tbpDepEsp
            // 
            this.tbpDepEsp.Controls.Add(this.listDep);
            this.tbpDepEsp.Controls.Add(this.cbxDep);
            this.tbpDepEsp.Controls.Add(this.btnBuscarDep);
            this.tbpDepEsp.Controls.Add(this.label1);
            this.tbpDepEsp.Location = new System.Drawing.Point(4, 29);
            this.tbpDepEsp.Name = "tbpDepEsp";
            this.tbpDepEsp.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDepEsp.Size = new System.Drawing.Size(795, 416);
            this.tbpDepEsp.TabIndex = 0;
            this.tbpDepEsp.Text = "Departamento especifico";
            this.tbpDepEsp.UseVisualStyleBackColor = true;
            this.tbpDepEsp.Leave += new System.EventHandler(this.tbpDepEsp_Leave);
            // 
            // cbxDep
            // 
            this.cbxDep.FormattingEnabled = true;
            this.cbxDep.Location = new System.Drawing.Point(27, 49);
            this.cbxDep.Name = "cbxDep";
            this.cbxDep.Size = new System.Drawing.Size(222, 28);
            this.cbxDep.TabIndex = 2;
            // 
            // btnBuscarDep
            // 
            this.btnBuscarDep.Location = new System.Drawing.Point(277, 46);
            this.btnBuscarDep.Name = "btnBuscarDep";
            this.btnBuscarDep.Size = new System.Drawing.Size(82, 33);
            this.btnBuscarDep.TabIndex = 1;
            this.btnBuscarDep.Text = "Buscar";
            this.btnBuscarDep.UseVisualStyleBackColor = true;
            this.btnBuscarDep.Click += new System.EventHandler(this.btnBuscarDep_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seleccione el departamento:";
            // 
            // tbpMujIng
            // 
            this.tbpMujIng.Controls.Add(this.listMujIng);
            this.tbpMujIng.Controls.Add(this.cbxIngles);
            this.tbpMujIng.Controls.Add(this.btnBuscarIng);
            this.tbpMujIng.Controls.Add(this.label2);
            this.tbpMujIng.Location = new System.Drawing.Point(4, 29);
            this.tbpMujIng.Name = "tbpMujIng";
            this.tbpMujIng.Padding = new System.Windows.Forms.Padding(3);
            this.tbpMujIng.Size = new System.Drawing.Size(795, 416);
            this.tbpMujIng.TabIndex = 1;
            this.tbpMujIng.Text = "Mujeres ingles";
            this.tbpMujIng.UseVisualStyleBackColor = true;
            this.tbpMujIng.Leave += new System.EventHandler(this.tbpMujIng_Leave);
            // 
            // tbpRanFecIng
            // 
            this.tbpRanFecIng.Controls.Add(this.dtpFeMay);
            this.tbpRanFecIng.Controls.Add(this.dtpFeMen);
            this.tbpRanFecIng.Controls.Add(this.listFecha);
            this.tbpRanFecIng.Controls.Add(this.btnFecha);
            this.tbpRanFecIng.Controls.Add(this.label3);
            this.tbpRanFecIng.Location = new System.Drawing.Point(4, 29);
            this.tbpRanFecIng.Name = "tbpRanFecIng";
            this.tbpRanFecIng.Size = new System.Drawing.Size(795, 416);
            this.tbpRanFecIng.TabIndex = 2;
            this.tbpRanFecIng.Text = "Rango fecha ingreso";
            this.tbpRanFecIng.UseVisualStyleBackColor = true;
            this.tbpRanFecIng.Leave += new System.EventHandler(this.tbpRanFecIng_Leave);
            // 
            // tbpHMEda
            // 
            this.tbpHMEda.Controls.Add(this.txtEdadMax);
            this.tbpHMEda.Controls.Add(this.txtEdadMin);
            this.tbpHMEda.Controls.Add(this.label7);
            this.tbpHMEda.Controls.Add(this.label6);
            this.tbpHMEda.Controls.Add(this.listEdad);
            this.tbpHMEda.Controls.Add(this.cbxDepEdad);
            this.tbpHMEda.Controls.Add(this.btnBuscarEdad);
            this.tbpHMEda.Controls.Add(this.label4);
            this.tbpHMEda.Location = new System.Drawing.Point(4, 29);
            this.tbpHMEda.Name = "tbpHMEda";
            this.tbpHMEda.Size = new System.Drawing.Size(795, 416);
            this.tbpHMEda.TabIndex = 3;
            this.tbpHMEda.Text = "Cantidad h/m por edad";
            this.tbpHMEda.UseVisualStyleBackColor = true;
            this.tbpHMEda.Leave += new System.EventHandler(this.tbpHMEda_Leave);
            // 
            // listDep
            // 
            this.listDep.FormattingEnabled = true;
            this.listDep.ItemHeight = 20;
            this.listDep.Location = new System.Drawing.Point(27, 89);
            this.listDep.Name = "listDep";
            this.listDep.Size = new System.Drawing.Size(744, 304);
            this.listDep.TabIndex = 3;
            // 
            // listMujIng
            // 
            this.listMujIng.FormattingEnabled = true;
            this.listMujIng.ItemHeight = 20;
            this.listMujIng.Location = new System.Drawing.Point(27, 88);
            this.listMujIng.Name = "listMujIng";
            this.listMujIng.Size = new System.Drawing.Size(744, 304);
            this.listMujIng.TabIndex = 7;
            // 
            // cbxIngles
            // 
            this.cbxIngles.FormattingEnabled = true;
            this.cbxIngles.Items.AddRange(new object[] {
            "A1",
            "A2",
            "B1",
            "B2",
            "C1"});
            this.cbxIngles.Location = new System.Drawing.Point(27, 48);
            this.cbxIngles.Name = "cbxIngles";
            this.cbxIngles.Size = new System.Drawing.Size(222, 28);
            this.cbxIngles.TabIndex = 6;
            // 
            // btnBuscarIng
            // 
            this.btnBuscarIng.Location = new System.Drawing.Point(277, 45);
            this.btnBuscarIng.Name = "btnBuscarIng";
            this.btnBuscarIng.Size = new System.Drawing.Size(82, 33);
            this.btnBuscarIng.TabIndex = 5;
            this.btnBuscarIng.Text = "Buscar";
            this.btnBuscarIng.UseVisualStyleBackColor = true;
            this.btnBuscarIng.Click += new System.EventHandler(this.btnBuscarIng_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Seleccione el nivel de ingles:";
            // 
            // listFecha
            // 
            this.listFecha.FormattingEnabled = true;
            this.listFecha.ItemHeight = 20;
            this.listFecha.Location = new System.Drawing.Point(27, 88);
            this.listFecha.Name = "listFecha";
            this.listFecha.Size = new System.Drawing.Size(744, 304);
            this.listFecha.TabIndex = 11;
            // 
            // btnFecha
            // 
            this.btnFecha.Location = new System.Drawing.Point(689, 42);
            this.btnFecha.Name = "btnFecha";
            this.btnFecha.Size = new System.Drawing.Size(82, 33);
            this.btnFecha.TabIndex = 9;
            this.btnFecha.Text = "Buscar";
            this.btnFecha.UseVisualStyleBackColor = true;
            this.btnFecha.Click += new System.EventHandler(this.btnFecha_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Seleccione los rangos de fecha:";
            // 
            // dtpFeMen
            // 
            this.dtpFeMen.Location = new System.Drawing.Point(27, 50);
            this.dtpFeMen.Name = "dtpFeMen";
            this.dtpFeMen.Size = new System.Drawing.Size(298, 26);
            this.dtpFeMen.TabIndex = 12;
            // 
            // dtpFeMay
            // 
            this.dtpFeMay.Location = new System.Drawing.Point(366, 49);
            this.dtpFeMay.Name = "dtpFeMay";
            this.dtpFeMay.Size = new System.Drawing.Size(298, 26);
            this.dtpFeMay.TabIndex = 13;
            // 
            // listEdad
            // 
            this.listEdad.FormattingEnabled = true;
            this.listEdad.ItemHeight = 20;
            this.listEdad.Location = new System.Drawing.Point(27, 88);
            this.listEdad.Name = "listEdad";
            this.listEdad.Size = new System.Drawing.Size(744, 304);
            this.listEdad.TabIndex = 7;
            // 
            // cbxDepEdad
            // 
            this.cbxDepEdad.FormattingEnabled = true;
            this.cbxDepEdad.Location = new System.Drawing.Point(239, 48);
            this.cbxDepEdad.Name = "cbxDepEdad";
            this.cbxDepEdad.Size = new System.Drawing.Size(222, 28);
            this.cbxDepEdad.TabIndex = 6;
            // 
            // btnBuscarEdad
            // 
            this.btnBuscarEdad.Location = new System.Drawing.Point(518, 43);
            this.btnBuscarEdad.Name = "btnBuscarEdad";
            this.btnBuscarEdad.Size = new System.Drawing.Size(82, 33);
            this.btnBuscarEdad.TabIndex = 5;
            this.btnBuscarEdad.Text = "Buscar";
            this.btnBuscarEdad.UseVisualStyleBackColor = true;
            this.btnBuscarEdad.Click += new System.EventHandler(this.btnBuscarEdad_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(235, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Departamento:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Edad min:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(124, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 20);
            this.label7.TabIndex = 10;
            this.label7.Text = "Edad max:";
            // 
            // txtEdadMin
            // 
            this.txtEdadMin.Location = new System.Drawing.Point(27, 51);
            this.txtEdadMin.Name = "txtEdadMin";
            this.txtEdadMin.Size = new System.Drawing.Size(76, 26);
            this.txtEdadMin.TabIndex = 11;
            // 
            // txtEdadMax
            // 
            this.txtEdadMax.Location = new System.Drawing.Point(128, 51);
            this.txtEdadMax.Name = "txtEdadMax";
            this.txtEdadMax.Size = new System.Drawing.Size(80, 26);
            this.txtEdadMax.TabIndex = 12;
            // 
            // FrmReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmReportes";
            this.Text = "Reportes";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmReportes_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.tbpDepEsp.ResumeLayout(false);
            this.tbpDepEsp.PerformLayout();
            this.tbpMujIng.ResumeLayout(false);
            this.tbpMujIng.PerformLayout();
            this.tbpRanFecIng.ResumeLayout(false);
            this.tbpRanFecIng.PerformLayout();
            this.tbpHMEda.ResumeLayout(false);
            this.tbpHMEda.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbpDepEsp;
        private System.Windows.Forms.TabPage tbpMujIng;
        private System.Windows.Forms.TabPage tbpRanFecIng;
        private System.Windows.Forms.TabPage tbpHMEda;
        private System.Windows.Forms.ComboBox cbxDep;
        private System.Windows.Forms.Button btnBuscarDep;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listDep;
        private System.Windows.Forms.ListBox listMujIng;
        private System.Windows.Forms.ComboBox cbxIngles;
        private System.Windows.Forms.Button btnBuscarIng;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFeMay;
        private System.Windows.Forms.DateTimePicker dtpFeMen;
        private System.Windows.Forms.ListBox listFecha;
        private System.Windows.Forms.Button btnFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEdadMax;
        private System.Windows.Forms.TextBox txtEdadMin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listEdad;
        private System.Windows.Forms.ComboBox cbxDepEdad;
        private System.Windows.Forms.Button btnBuscarEdad;
        private System.Windows.Forms.Label label4;
    }
}