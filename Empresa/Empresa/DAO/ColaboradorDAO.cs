﻿using Empresa.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.DAO
{
    class ColaboradorDAO
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta un Colaborador a la BD
        /// </summary>
        /// <param name="c"></param> recibe un colaborador
        public void InsertarDatos(Colaborador c)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO colaboradores (cedula, nombre, genero, fecha_nacimiento, edad, fecha_ingreso, nivel_ingles, id_departamento, id_puesto) VALUES ('" + c.sgCedula + "', '" + c.sgNombre + "', '" + c.sgGenero + "', '" + c.sgFechaNacimiento + "', '" + c.sgEdad +"', '" + c.sgFechaIngreso + "', '" + c.sgNivelIngles + "', '" + c.sgDepartamento.sgId + "', '" + c.sgPuesto.sgId +"')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Modifica un Colaborador
        /// </summary>
        /// <param name="c"></param> recibe el id de un colaborador
        public void ModificarDatos(Colaborador c)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("UPDATE colaboradores SET nombre = '" + c.sgNombre + "', fecha_ingreso = '" + c.sgFechaIngreso + "', nivel_ingles = '" + c.sgNivelIngles + "', id_departamento = '" + c.sgDepartamento.sgId + "', id_puesto = '" + c.sgPuesto.sgId + "'   WHERE cedula = '" + c.sgCedula + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Consulta un colaborador en especifico
        /// </summary>
        /// <param name="cedula"></param> recibe la cedula del colaborador
        /// <returns></returns> retorna un colaborador
        public Colaborador ConsultarDatos(string cedula)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, genero, fecha_nacimiento, edad, fecha_ingreso, nivel_ingles, id_departamento, id_puesto FROM colaboradores WHERE cedula = '" + cedula + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    Colaborador c = new Colaborador();
                    DepartamentoDAO ddao = new DepartamentoDAO();
                    PuestoDAO pdao = new PuestoDAO();
                    c.sgId = dr.GetInt32(0);
                    c.sgCedula = dr.GetString(1);
                    c.sgNombre = dr.GetString(2);
                    c.sgGenero = dr.GetChar(3);
                    c.sgFechaNacimiento = dr.GetDateTime(4);
                    c.sgEdad = dr.GetInt32(5);
                    c.sgFechaIngreso = dr.GetDateTime(6);
                    c.sgNivelIngles = dr.GetString(7);
                    c.sgDepartamento = ddao.ConsultarDepEsp(dr.GetInt32(8));
                    c.sgPuesto = pdao.ConsultarPueEsp(dr.GetInt32(9));
                    return c;
                }

            }
            conexion.Close();

            return null;
        }

        /// <summary>
        /// Consulta de colaboradores Femeninos segun su nivel de ingles
        /// </summary>
        /// <param name="nivIng"></param> recibe el nivel de ingles 
        /// <returns></returns> retorna una lista de reportes de colaboradores
        public List<object> ConsultaNivIngFList(string nivIng)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<object> lista = new List<object>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT c.nombre, c.genero, c.nivel_ingles, d.nombre, p.nombre FROM departamentos as d JOIN colaboradores as c ON d.id = c.id_departamento JOIN puestos as p ON p.id = c.id_puesto WHERE c.genero = 'F' and c.nivel_ingles = '" + nivIng + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarNivIng(dr));
                }
            }
            conexion.Close();

            return lista;
        }

        /// <summary>
        /// Castea lo recibido por la BD
        /// </summary>
        /// <param name="dr"></param> DataReadear de la BD
        /// <returns></returns> retorna un reporte para la consulta
        private String cargarNivIng(NpgsqlDataReader dr)
        {
            char sex;
            string consulta = "";
            consulta += "Nombre: " + dr.GetString(0);
            if (dr.GetChar(1) == 'M')
            {
                consulta += " Genero: Masculino";  
            }
            else if (dr.GetChar(1) == 'F')
            {
                consulta += " Genero: Femenino";

            }
            consulta += " Nivel de ingles: " + dr.GetString(2);
            consulta += " Departamento: " + dr.GetString(3);
            consulta += " Puesto: " + dr.GetString(4);


            return consulta;
        }

        /// <summary>
        /// Consulta colaboradores por fecha de ingreso
        /// </summary>
        /// <param name="min"></param> fecha minima de rango
        /// <param name="may"></param> fecha maxima de rango
        /// <returns></returns> retorna una lista de reportes de colaboradores
        public List<object> ConsultaFechas(DateTime min, DateTime may)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<object> lista = new List<object>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT c.nombre, c.fecha_ingreso FROM colaboradores as c WHERE c.fecha_ingreso between '" + min + "' and '" + may + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarFech(dr));
                }
            }
            conexion.Close();

            return lista;
        }

        /// <summary>
        /// Castea lo recibido por BD
        /// </summary>
        /// <param name="dr"></param> DataReadear de la BD
        /// <returns></returns> retorna un reporte para la consulta
        private String cargarFech(NpgsqlDataReader dr)
        {
            string consulta = "";
            consulta += "Nombre: " + dr.GetString(0);
            consulta += " Fecha ingreso: " + dr.GetDateTime(1).ToShortDateString();

            return consulta;
        }

        /// <summary>
        /// Consulta colaboradores segun rango de edad, genero y un departamento 
        /// </summary>
        /// <param name="min"></param> edad minima
        /// <param name="max"></param> edad maxima
        /// <param name="dep"></param> id departamento
        /// <returns></returns> retorna una lista de reportes de colabaradores
        public List<object> ConsultaGeEdDep(int min, int max, int dep)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<object> lista = new List<object>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT c.genero, count(*) FROM colaboradores as c JOIN departamentos as d ON c.id_departamento = d.id WHERE c.edad between '" + min + "' and '" + max + "' and d.id = '" + dep + "' group by genero", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarGeEdDep(dr));
                }
            }
            conexion.Close();

            return lista;
        }

        /// <summary>
        /// Castea lo recibido por BD 
        /// </summary>
        /// <param name="dr"></param> DataReadear de la BD
        /// <returns></returns> retorna un reporte para la consulta
        private String cargarGeEdDep(NpgsqlDataReader dr)
        {
            string consulta = "";
            if (dr.GetChar(0) == 'M')
            {
                consulta += " Genero: Masculino";
            }
            else if (dr.GetChar(0) == 'F')
            {
                consulta += " Genero: Femenino";

            }
            consulta += " Cantidad: " + dr.GetInt32(1);

            return consulta;
        }

    }
}
