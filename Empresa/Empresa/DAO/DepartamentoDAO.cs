﻿using Empresa.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.DAO
{
    class DepartamentoDAO
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Consulta un departamento en la BD
        /// </summary>
        /// <param name="id"></param> recibe el id para buscar en BD
        /// <returns></returns> retorna un Departamento
        public Departamento ConsultarDepEsp(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre FROM departamentos WHERE id = '" + id + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    return CargarDepartamento(dr);
                }

            }
            conexion.Close();

            return null;
        }

        /// <summary>
        /// Consulta un departamento en la BD
        /// </summary>
        /// <returns></returns> retorna un list de Departamentos
        public List<Departamento> ConsultarDepartamentos()
        {
            conexion = Conexion.ConexionS(); 
            conexion.Open();
            List<Departamento> lista = new List<Departamento>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre FROM departamentos ", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(CargarDepartamento(dr));
                }
            }
            conexion.Close();

            return lista;
        }

        /// <summary>
        /// Castea lo recibido desde la BD a un Departamento
        /// </summary>
        /// <param name="dr"></param> DataReadear de la BD
        /// <returns></returns> retorna un Departamento
        private Departamento CargarDepartamento(NpgsqlDataReader dr)
        {
            Departamento d = new Departamento();
            d.sgId = dr.GetInt32(0);
            d.sgNombre = dr.GetString(1);
            return d;

        }

        /// <summary>
        /// Consulta de colaboradores de un departamento
        /// </summary>
        /// <param name="id"></param> recibe el id del departamento
        /// <returns></returns> retorna una lista de reportes de colaboradores
        public List<object> ConsultaDepList(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<object> lista = new List<object>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT c.cedula, c.nombre, c.edad, c.fecha_ingreso, d.nombre, p.abreviacion FROM departamentos as d JOIN colaboradores as c ON d.id = c.id_departamento JOIN puestos as p ON p.id = c.id_puesto WHERE d.id = '" + id + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarDepEsp(dr));
                }
            }
            conexion.Close();

            return lista;
        }

        /// <summary>
        /// Castea lo recibido por BD 
        /// </summary>
        /// <param name="dr"></param> DataReadear de la BD
        /// <returns></returns> retorna un reporte para la consulta
        private String cargarDepEsp(NpgsqlDataReader dr)
        {
            string consulta = "";
            consulta += "Cedula: " + dr.GetString(0);
            consulta += " Nombre: " + dr.GetString(1);
            consulta += " Edad: " + dr.GetInt32(2);
            consulta += " Fecha ingreso: " + dr.GetDateTime(3).ToShortDateString();
            consulta += " Departamento: " + dr.GetString(4);
            consulta += " Puesto: " + dr.GetString(5);

            return consulta;
        }

    }
}
