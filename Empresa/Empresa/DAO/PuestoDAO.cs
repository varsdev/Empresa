﻿using Empresa.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.DAO
{
    class PuestoDAO
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Consulta un puesto a la BD
        /// </summary>
        /// <param name="id"></param> recibe el id para buscar en BD
        /// <returns></returns> retorna un Puesto
        public Puesto ConsultarPueEsp(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, abreviacion FROM puestos WHERE id = '" + id + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                if (dr.Read())
                {                  
                    return CargarPuesto(dr);
                }

            }
            conexion.Close();

            return null;
        }

        /// <summary>
        /// Consulta un puesto a la BD
        /// </summary>
        /// <returns></returns> retorna un list de Puestos
        public List<Puesto> ConsultarPuestos()
        {
            conexion = Conexion.ConexionS(); ;
            conexion.Open();
            List<Puesto> lista = new List<Puesto>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, abreviacion FROM puestos ", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(CargarPuesto(dr));
                }
            }
            conexion.Close();

            return lista;
        }

        /// <summary>
        /// Castea lo recibido desde la BD a un Puesto
        /// </summary>
        /// <param name="dr"></param> es el DataReadear de la BD
        /// <returns></returns> retorna un Puesto
        private Puesto CargarPuesto(NpgsqlDataReader dr)
        {
            Puesto p = new Puesto();
            p.sgId = dr.GetInt32(0);
            p.sgNombre = dr.GetString(1);
            p.sgAbreviacion = dr.GetString(2);
            return p;

        }


    }
}
