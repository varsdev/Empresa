﻿using Empresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.BO
{
    class ColaboradorBO
    {           
        /// <summary>
        /// Calcula la edad de una persona
        /// </summary>
        /// <param name="fNaci"></param> fecha de nacimiento de una persona
        /// <returns></returns> un entero con la edad de la persona
        public int calcEdad(DateTime fNaci)
        {
            DateTime nacimiento = fNaci;
            int edadB = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
            return edadB;
        }


    }
}
