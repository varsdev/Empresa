Create Database empresa;

Create table departamentos
(
	id ser  ial primary key,
	nombre text not null,
	CONSTRAINT unq_departamentos_nombre UNIQUE (nombre)
);

Create table puestos
(
	id serial primary key,
	nombre text not null,
	abreviacion text not null,
	CONSTRAINT unq_puestos_nombre UNIQUE (nombre)
);

Create table colaboradores
(
	id serial primary key,
	cedula text not null,
	nombre text not null,
	genero char default 'M',
	fecha_nacimiento date not null,
	edad int not null,
	fecha_ingreso date not null,
	nivel_ingles text not null,
	id_departamento int not null,
	id_puesto int not null,
	foreign key(id_departamento) references departamentos(id),
	foreign key(id_puesto) references puestos(id),
	CONSTRAINT unq_colaboradores_cedula UNIQUE (cedula)	
);

--Consulta 1
SELECT c.cedula, c.nombre, c.edad, c.fecha_ingreso, d.nombre, p.abreviacion 
FROM departamentos as d
JOIN colaboradores as c ON d.id = c.id_departamento 
JOIN puestos as p ON p.id = c.id_puesto
WHERE d.id = 3

--Consulta 2
SELECT c.nombre, c.genero, c.nivel_ingles, d.nombre, p.nombre 
FROM departamentos as d
JOIN colaboradores as c ON d.id = c.id_departamento 
JOIN puestos as p ON p.id = c.id_puesto
WHERE c.genero = 'F' and c.nivel_ingles = 'A2'

--Consulta 3
SELECT c.nombre, c.fecha_ingreso 
FROM colaboradores as c
WHERE c.fecha_ingreso between '20160101' and '20200630';

--Consulta 4
select c.genero, count(*)
FROM colaboradores as c
JOIN departamentos as d ON c.id_departamento = d.id
WHERE c.edad between 10 and 25 and d.id = 3 
group by genero



