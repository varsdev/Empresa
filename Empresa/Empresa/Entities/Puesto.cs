﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.Entities
{
    class Puesto
    {
        private int id;
        private string nombre;
        private string abreviacion;

        public int sgId
        {
            set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }

        public string sgNombre
        {
            set
            {
                nombre = value;
            }
            get
            {
                return nombre;
            }
        }

        public string sgAbreviacion
        {
            set
            {
                abreviacion = value;
            }
            get
            {
                return abreviacion;
            }
        }

        public override string ToString()
        {
            return nombre + " " + "(" + abreviacion + ")";
        }

    }
}
