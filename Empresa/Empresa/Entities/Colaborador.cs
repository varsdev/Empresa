﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.Entities
{
    class Colaborador
    {
		private int id;
		private string cedula;
		private string nombre;
		private char genero;
		private DateTime fechaNacimiento;
		private int edad;
		private DateTime fechaIngreso;
		private string nivelIngles;
		private Departamento dep;
		private Puesto pue;


		public int sgId
        {
            set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }

        public string sgCedula
        {
            set
            {
                cedula = value;
            }
            get
            {
                return cedula;
            }
        }

        public string sgNombre
        {
            set
            {
                nombre = value;
            }
            get
            {
                return nombre;
            }
        }

        public char sgGenero
        {
            set
            {
                genero = value;
            }
            get
            {
                return genero;
            }
        }

        public string gGenero
        {
            get
            {
                if (genero == 'M')
                {
                    return "Masculino";
                }
                else if (genero == 'F')
                {
                    return "Femenino";
                }
                else
                {
                    return "Indefinido";
                }
            }
        }

        public DateTime sgFechaNacimiento
        {
            set
            {
                fechaNacimiento = value;
            }
            get
            {
                return fechaNacimiento;
            }
        }

        public int sgEdad
        {
            set
            {
                edad = value;
            }
            get
            {
                return edad;
            }
        }

        public DateTime sgFechaIngreso
        {
            set
            {
                fechaIngreso = value;
            }
            get
            {
                return fechaIngreso;
            }
        }

        public string sgNivelIngles
        {
            set
            {
                nivelIngles = value;
            }
            get
            {
                return nivelIngles;
            }
        }

        public Departamento sgDepartamento
        {
            set
            {
                dep = value;
            }
            get
            {
                return dep;
            }
        }

        public Puesto sgPuesto
        {
            set
            {
                pue = value;
            }
            get
            {
                return pue;
            }
        }

        public override string ToString()
        {
            return cedula + " " + nombre + " " + gGenero + " " + fechaNacimiento.ToShortDateString() + " " + sgEdad + " " + fechaIngreso.ToShortDateString() + " " + nivelIngles + " " + dep + " " + pue;
        }


    }
}
