﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.Entities
{
    class Departamento
    {
        private int id;
        private string nombre;


        public int sgId
        {
            set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }

        public string sgNombre
        {
            set
            {
                nombre = value;
            }
            get
            {
                return nombre;
            }
        }

        public override string ToString()
        {
            return nombre;
        }

    }
}
